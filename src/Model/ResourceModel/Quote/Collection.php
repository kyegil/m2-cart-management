<?php

namespace Kyegil\CartManagement\Model\ResourceModel\Quote;


/**
 * Class Collection
 * @package Kyegil\CartManagement\Model\ResourceModel\Quote
 */
class Collection extends \Magento\Quote\Model\ResourceModel\Quote\Collection {
    protected $_idFieldName = 'entity_id';
}

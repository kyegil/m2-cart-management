<?php

namespace Kyegil\CartManagement\Controller\Adminhtml\Quote;


use Kyegil\CartManagement\Model\ResourceModel\Quote\Collection;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Quote\Api\CartRepositoryInterface;
use Kyegil\CartManagement\Model\ResourceModel\Quote\CollectionFactory;
use Magento\Quote\Model\Quote;
use Magento\Ui\Component\MassAction\Filter;

class massActivate extends Action implements HttpPostActionInterface {
    /**
     * Authorization level
     */
    const ADMIN_RESOURCE = 'Magento_Sales::sales_order';

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var Filter
     */
    protected $filter;

    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        CartRepositoryInterface $cartRepository
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->cartRepository = $cartRepository;
        parent::__construct( $context );
    }

    /**
     * @return Redirect
     * @throws NotFoundException
     * @throws LocalizedException
     */
	public function execute(): Redirect {
        if (!$this->getRequest()->isPost()) {
            throw new NotFoundException(__('Page not found'));
        }
        /** @var Collection $collection */
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $quotesActivated = 0;
        /** @var Quote $quote */
        foreach ($collection->getItems() as $quote) {
            if (!$quote->getIsActive()) {
                $quote->setIsActive(true);
                $this->cartRepository->save($quote);
                $quotesActivated++;
            }
        }

        if ($quotesActivated) {
            if ($quotesActivated == 1) {
                $this->messageManager->addSuccessMessage(
                    __('1 cart has been re-activated.')
                );
            }
            else {
                $this->messageManager->addSuccessMessage(
                    __('%1 carts have been re-activated.', $quotesActivated)
                );
            }
        }
        return $this->resultFactory->create( ResultFactory::TYPE_REDIRECT)->setPath('cart_management/index/index');	}
}

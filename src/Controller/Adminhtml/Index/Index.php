<?php

namespace Kyegil\CartManagement\Controller\Adminhtml\Index;


use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index
 * @package Kyegil\CartManagement\Controller\Adminhtml\Index
 */
class Index extends Action implements HttpGetActionInterface {

    /** @var PageFactory */
    private $pageFactory;

    public function __construct(
        Context $context,
        PageFactory $rawFactory
    ) {
        $this->pageFactory = $rawFactory;
        parent::__construct($context);

    }

    /**
     * Add the main Admin Grid page
     *
     * @return Page
     */
    public function execute(): Page
    {
        $resultPage = $this->pageFactory->create();
        $resultPage->setActiveMenu('Kyegil_CartManagement::defaults_index');
        $resultPage->getConfig()->getTitle()->prepend(__('Active Quotes'));

        return $resultPage;
    }}

<?php

namespace Kyegil\CartManagement\Ui\Component\Quote\Listing\Column;


use Magento\Framework\Url;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Actions
 * @package Kyegil\CartManagement\Ui\Component\Quote\Listing\Column
 */
class Actions extends Column {
    /**
     * @var Url
     */
    protected $urlBuilder;
    /**
     * @var string
     */
    protected $viewUrl;

    /**
     * Actions constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Url $urlBuilder,
        string $viewUrl,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->viewUrl = $viewUrl;
        parent::__construct( $context, $uiComponentFactory, $components, $data );
    }

    /**
     * @param array $dataSource
     *
     * @return mixed
     */
    public function prepareDataSource( array $dataSource ) {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $name = $this->getData('name');
                if (isset($item['entity_id'])) {
                    $item[$name]['view']   = [
                        'href'  => $this->urlBuilder->getUrl($this->viewUrl, ['id' => $item['entity_id']]),
                        'target' => '_blank',
                        'label' => __('View on Frontend')
                    ];
                }
            }
        }
        return $dataSource;
    }
}

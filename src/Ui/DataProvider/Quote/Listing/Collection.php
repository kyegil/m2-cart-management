<?php

namespace Kyegil\CartManagement\Ui\DataProvider\Quote\Listing;

use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;

/**
 * Class Collection
 * @package Kyegil\CartManagement\Ui\DataProvider\Quote\Listing
 */
class Collection extends SearchResult {

    /**
     * Override _initSelect to add custom columns
     *
     * @return void
     */
    protected function _initSelect()
    {
        $this->addFilterToMap('entity_id', 'main_table.entity_id');
        parent::_initSelect();
    }
}

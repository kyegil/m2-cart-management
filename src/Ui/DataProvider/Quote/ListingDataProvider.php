<?php

namespace Kyegil\CartManagement\Ui\DataProvider\Quote;

use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider;

/**
 * Data Provider for the Quote Grid
 *
 * @package Kyegil\CartManagement\Ui\DataProvider\Quote
 */
class ListingDataProvider extends DataProvider {

}
